package io.catalyte.training.services;

import io.catalyte.training.entities.Pet;
import io.catalyte.training.entities.Vaccination;
import io.catalyte.training.exceptions.BadDataResponse;
import io.catalyte.training.exceptions.ResourceNotFound;
import io.catalyte.training.exceptions.ServiceUnavailable;
import io.catalyte.training.repositories.PetRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Example;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class PetServiceImplTest {

    @Mock
    PetRepository mockPetRepository;

    @InjectMocks
    PetServiceImpl petServiceImpl;

    // member variables for expected results
    Pet testPet;
    Vaccination testVaccination;
    List<Pet> testList = new ArrayList<>();


    @BeforeEach
    public void setUp(){
        //initialize mocks
        MockitoAnnotations.openMocks(this);

        //set up test Pet
        testPet = new Pet("Whopper", "Dog", 1);
        testPet.setId(1L);
        testList.add(testPet);

        //set up test Vac
        testVaccination = new Vaccination("Rabies", Date.valueOf("2021-01-08"), testPet);
        testVaccination.setId(1L);


        when(mockPetRepository.findById(any(Long.class))).thenReturn(Optional.of(testList.get(0)));
        when(mockPetRepository.saveAll(anyCollection())).thenReturn(testList);
        when(mockPetRepository.save(any(Pet.class))).thenReturn(testList.get(0));
        when(mockPetRepository.findAll()).thenReturn(testList);
        when(mockPetRepository.findAll(any(Example.class))).thenReturn(testList);
    }


    @Test
    public void getAllPets() {
        List<Pet> result = petServiceImpl.queryPets(new Pet());
        assertEquals(testList, result);
    }

    @Test
    public void getAllPetsWithSample(){
        List<Pet> result = petServiceImpl.queryPets(testPet);
        assertEquals(testList, result);
    }

    @Test
    public void getAllPetsDBError(){
        // set repo to trigger Data Access Exception
        when(mockPetRepository.findAll()).thenThrow(EmptyResultDataAccessException.class);
        assertThrows(ServiceUnavailable.class,
                () -> petServiceImpl.queryPets(new Pet()));
    }

    @Test
    public void getPet(){
        Pet result = petServiceImpl.getPet(1L);
        assertEquals(testPet, result);
    }

    @Test
    public void getPetDBError(){
        when(mockPetRepository.findById(anyLong())).thenThrow(EmptyResultDataAccessException.class);
        assertThrows(ServiceUnavailable.class,
                () -> petServiceImpl.getPet(1L));
    }

    @Test
    public void getPetNotFound(){
        when(mockPetRepository.findById(any(Long.class))).thenReturn(Optional.empty());
        assertThrows(ResourceNotFound.class,
                () -> petServiceImpl.getPet(1L));
    }

    @Test
    public void addPets(){
        List<Pet> result = petServiceImpl.addPets(testList);
        assertEquals(testList, result);
    }

    @Test
    public void addPetsDBError(){
        when(mockPetRepository.saveAll(anyCollection())).thenThrow(ServiceUnavailable.class);
        assertThrows(ServiceUnavailable.class,
                () -> petServiceImpl.addPets(testList));
    }

    @Test
    public void addPet(){
        Pet result = petServiceImpl.addPet(testPet);
        assertEquals(testPet, result);
    }

    @Test
    public void addPetDBError(){
        when(mockPetRepository.save(any(Pet.class))).thenThrow(ServiceUnavailable.class);
        assertThrows(ServiceUnavailable.class,
                () -> petServiceImpl.addPet(testPet));

    }

    @Test
    public void updatePetById(){
        Pet result = petServiceImpl.updatePetById(1L, testPet);
        assertEquals(testPet, result);
    }

    @Test
    public void updatePetByIdDBError(){
        when(mockPetRepository.save(any(Pet.class))).thenThrow(ServiceUnavailable.class);
        assertThrows(ServiceUnavailable.class,
                () -> petServiceImpl.updatePetById(1L, testPet));
    }

    @Test
    public void updatePetByIdNotFound() {
        when(mockPetRepository.findById(anyLong())).thenReturn(Optional.empty());
        assertThrows(ResourceNotFound.class,
                () -> petServiceImpl.updatePetById(1L, testPet));
    }

    @Test
    public void updatePetBadData() {
        when(mockPetRepository.save(any(Pet.class))).thenReturn(testPet);
        assertThrows(BadDataResponse.class,
                () -> petServiceImpl.updatePetById(2L, testPet));
    }

    @Test
    public void deletePet() {
        when(mockPetRepository.existsById(anyLong())).thenReturn(true);
        petServiceImpl.deletePet(1L);
        verify(mockPetRepository).deleteById(any());
    }

    @Test
    public void deletePetDBError() {
        when(mockPetRepository.existsById(anyLong())).thenReturn(true);
        doThrow(ServiceUnavailable.class).when(mockPetRepository).deleteById(anyLong());
        assertThrows(ServiceUnavailable.class,
                () -> petServiceImpl.deletePet(1L));
    }

    @Test
    public void deletePetNotFound() {
        when(mockPetRepository.existsById(anyLong())).thenReturn(false);
        assertThrows(ResourceNotFound.class,
                () -> petServiceImpl.deletePet(1L));
    }
}